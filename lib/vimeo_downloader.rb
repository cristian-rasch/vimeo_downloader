require_relative "vimeo_downloader/version"
require_relative "vimeo_downloader/video_downloader"
require_relative "vimeo_downloader/aws/s3/client"

module VimeoDownloader
  def self.upload_to_s3_videos_for(user)
    s3_cli = AWS::S3::Client.new(user)

    VideoDownloader.new(user).download_videos do |filename|
      path = File.expand_path(filename)
      s3_cli.upload(path)
    end
  end
end
