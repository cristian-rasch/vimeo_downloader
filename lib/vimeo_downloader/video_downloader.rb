require "mechanize"
require "logger"
require "open3"
require "fileutils"

module VimeoDownloader
  class VideoDownloader
    VIDEO_PATH_REGEXP = %r(\A/(\d+)\z)
    VIMEO_DOWNLOADER_SCRIPT = File.expand_path("../../bin/vimeo_downloader.sh", __dir__)

    def initialize(user)
      @user = user

      @agent = Mechanize.new
      @agent.user_agent_alias = "Mac Safari"

      @logger = Logger.new(STDOUT)
      env = ENV.fetch("RACK_ENV", "development")
      @logger.level = Logger::ERROR unless env == "development"
    end

    def download_videos
      next_page_url = "https://vimeo.com/#{@user}/videos"

      begin
        @logger.info "Downloading videos from page #{next_page_url}"
        page = @agent.get(next_page_url)
        video_ids = page.links_with(href: VIDEO_PATH_REGEXP).
                         map(&:href).
                         map! { |path| path[VIDEO_PATH_REGEXP, 1] }
        @logger.debug "Scraped video IDs: #{video_ids.inspect}"

        video_ids.each do |id|
          @logger.info "Downloading video with ID #{id}"

          output, status = Open3.capture2e("#{VIMEO_DOWNLOADER_SCRIPT} #{id}")
          if status.success?
            video = output[/Video \d+ saved to ([^\n]+)/, 1]
            yield(video)
            FileUtils.rm_rf Dir["*.flv"]
          else
            @logger.error "Could not download video with ID #{id}"
            @logger.debug output
          end
        end

        if current_page_link = page.at("#pagination a.selected") and next_page_li = current_page_link.parent.next_element
          next_page_url = next_page_li.at("a")["href"]
        else
          next_page_url = nil
        end
      end while next_page_url
    end
  end
end
