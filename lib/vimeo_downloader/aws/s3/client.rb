require File.expand_path("../../dotenv", __dir__)
require "aws-sdk-resources"
require "logger"

module VimeoDownloader
  module AWS
    module S3
      class Client
        MAX_RETRIES = 3

        def initialize(user)
          @user = user

          s3 = Aws::S3::Resource.new
          bucket_name = ENV.fetch("AWS_BUCKET_NAME", "vimeo_videos")
          @bucket = s3.bucket(bucket_name)
          s3.create_bucket(bucket: bucket_name) unless @bucket.exists?

          @logger = Logger.new(STDOUT)
          env = ENV.fetch("RACK_ENV", "development")
          @logger.level = Logger::ERROR unless env == "development"
        end

        def upload(path)
          filename = File.basename(path)
          key = File.join(@user, filename)
          object = @bucket.object(key)
          retries = -1
          success = false

          begin
            @logger.debug "Uploading video '#{filename}', retry: ##{retries + 1}"
            success = object.upload_file(path)
          rescue Aws::S3::Errors::ServiceError
            retries += 1

            if !object.exists? && retries <= MAX_RETRIES
              retry
            else
              @logger.error "Can't upload video '#{filename}'"
            end
          else
            @logger.debug "Uploaded video #{File.basename(path)} to #{key}"
          end

          [success, key]
        end

        def delete(key)
          object = @bucket.object(key)
          object.delete if object.exists?
        end
      end
    end
  end
end

