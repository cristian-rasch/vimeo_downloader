$LOAD_PATH.unshift File.expand_path("../../lib", __FILE__)
require "vimeo_downloader"

require "minitest/autorun"
require "minitest/pride"

ENV["RACK_ENV"] = "test"
