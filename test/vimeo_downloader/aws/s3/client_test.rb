require File.expand_path("test/test_helper")

module VimeoDownloader
  module AWS
    module S3
      class ClientTest < Minitest::Test
        def test_it_uploads_files_to_s3
          s3_cli = Client.new("user41230916")
          success, key = s3_cli.upload(File.expand_path("test/fixtures/red-wine.jpg"))
          assert success
          s3_cli.delete(key)
        end
      end
    end
  end
end
