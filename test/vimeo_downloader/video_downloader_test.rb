require_relative "../test_helper"

module VimeoDownloader
  class VideoDownloaderTest < Minitest::Test
    def test_it_yields_one_video_at_a_time
      downloader = VideoDownloader.new("user41230916")
      video_download_count = 0

      downloader.download_videos do |filename|
        assert File.exist?(filename)
        video_download_count += 1
        break
      end

      assert_equal 1, video_download_count
    end
  end
end
